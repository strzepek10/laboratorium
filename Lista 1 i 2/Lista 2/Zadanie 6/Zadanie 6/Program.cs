﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zadanie_6;
using Zadanie6;

namespace Zadanie6
{
    class Program
    {
        static void Main(string[] args)
        {
            Kuchnia kuchnia1 = new Kuchnia("lodowka", 25, "schładza", 45);
            Kuchnia kuchnia2 = new Kuchnia("mikrofalowka", 88, "podgrzewa", 62);
            Kuchnia kuchnia3 = new Kuchnia("zmywarka", 1, "czysci", 70);
            Console.WriteLine("{0} {1} {2} {3}", kuchnia1.nazwa, kuchnia1.zuzyciePradu, kuchnia1.corobi, kuchnia1.jakglosna);
            Console.WriteLine("{0} {1} {2} {3}", kuchnia2.nazwa, kuchnia2.zuzyciePradu, kuchnia2.corobi, kuchnia2.jakglosna);
            Console.WriteLine("{0} {1} {2} {3}", kuchnia3.nazwa, kuchnia3.zuzyciePradu, kuchnia3.corobi, kuchnia3.jakglosna);


            Console.WriteLine("{0} {1} {2}" ,kuchnia1.IleZuzywa(), kuchnia2.IleZuzywa(), kuchnia3.IleZuzywa());
            Console.WriteLine("{0} {1} {2}", kuchnia1.JakieJestJejUzycie(), kuchnia2.JakieJestJejUzycie(), kuchnia3.JakieJestJejUzycie());

        }
    }
}