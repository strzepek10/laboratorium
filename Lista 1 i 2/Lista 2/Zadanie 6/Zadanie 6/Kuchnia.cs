﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie_6
{
    internal class Kuchnia
    {
        public string nazwa;
        public int zuzyciePradu;
        public string corobi;
        public int jakglosna;

        public Kuchnia(string nazwa1, int zuzyciePradu1, string corobi1, int jakglosna1)
        {
            zuzyciePradu = zuzyciePradu1;
            corobi = corobi1;
            jakglosna = jakglosna1;
            nazwa = nazwa1;
        }

        public bool IleZuzywa()
        {
            if(zuzyciePradu <= 50)
            {
                return true;
            }
            return false;
        }

        public string JakieJestJejUzycie()
        {

            if (corobi == "schładza")
            {
                Console.WriteLine("to fajnie");
                return "";
            }
            else if (corobi == "podgrzewa")
            {
                Console.WriteLine("to dobrze");
                return "";
            }
            else if (corobi == "czysci")
            {
                Console.WriteLine("to niedobrze");
                return "";
            }
            return "ok";
        }




    }
}
