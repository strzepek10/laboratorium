﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie5
{
    class Aquarium
    {
        public string swiming;
        public string eating;
        public string lookingGood;

        public Aquarium(string aswiming, string aeating, string alookingGood)
        {
            swiming = aswiming;
            eating = aeating;
            lookingGood = alookingGood;
        }

        public Aquarium(Aquarium aquarium)
        {
            swiming = aquarium.swiming;
            eating = aquarium.eating;
            lookingGood = aquarium.lookingGood;
        }

        public Aquarium()
        {

        }
    }

    internal class Domestic
    {
        public string eating;
        public int sleepTime;
        public string favoriteActivity;
    public Domestic(int asleepTime, string aeating, string afavoriteActivity)
        {
            sleepTime = asleepTime;
            eating = aeating;
            favoriteActivity = afavoriteActivity;
        }

    public Domestic(Domestic domestic)
        {
            sleepTime = domestic.sleepTime;
            eating = domestic.eating;
            favoriteActivity = domestic.favoriteActivity;
        }

    public Domestic()
        {
            
        }
    }
}
