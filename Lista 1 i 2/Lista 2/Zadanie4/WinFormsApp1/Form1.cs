namespace WinFormsApp1
{
    public partial class Form1 : Form
    {
        // variables

        int correctAnswer;
        int questionNumber = 1;
        int score;
        int percentage;
        int totalQuestions;
        public Form1()
        {
            InitializeComponent();

            askQuestion(questionNumber);

            totalQuestions = 5;
        }

        private void checkAnswerEvent(object sender, EventArgs e)
        {
            var senderObject = (Button)sender;

            int buttonTag = Convert.ToInt32(senderObject.Tag);

             if(buttonTag == correctAnswer)
            {
                score++;
            }

             if(questionNumber == totalQuestions)
            {

                percentage = (int)Math.Round((double)(score * 100) / totalQuestions );



                score = 0;
                questionNumber = 0;
                askQuestion(questionNumber);



            }

            questionNumber++;
            askQuestion(questionNumber);


        }


        private void askQuestion(int qnum)
        {

            switch (qnum)
            {
                case 1:

                    lblQuestion.Text = "Jak si� dzi� czujesz?";
                    button1.Text = "S�abo";
                    button2.Text = "Super";
                    correctAnswer = 1;
                    break;

                case 2:

                    lblQuestion.Text = "Jak si� trzymasz?";
                    button1.Text = "S�abo";
                    button2.Text = "Super";
                    correctAnswer = 1;
                    break;

                case 3:

                    lblQuestion.Text = "Jak tam?";
                    button1.Text = "S�abo";
                    button2.Text = "Super";
                    correctAnswer = 1;
                    break;

                case 4:

                    lblQuestion.Text = "Jak leci?";
                    button1.Text = "S�abo";
                    button2.Text = "Super";
                    correctAnswer = 1;
                    break;
                case 5:

                    lblQuestion.Text = "Sprawdz wynik";
                    button1.Text = "Sprawdz wynik";
                    button2.Text = "Sprawdz wynik";
                    correctAnswer = 1;
                    button1.Visible = false;
                    button2.Visible = false;
                    break;


            }


        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            if (score <= 2)
            {
                Graphics g = e.Graphics;
                Brush myDrawingBrush = new SolidBrush(Color.Cyan);
                Pen MyDrawingPen = new Pen(Color.Cyan);
                g.FillEllipse(myDrawingBrush, 60, 20, 20, 20);
                g.FillEllipse(myDrawingBrush, 115, 20, 20, 20);
                g.DrawLine(MyDrawingPen, new PointF(0, 50), new PointF (50, 100));
                g.DrawLine(MyDrawingPen, new PointF(50, 100), new PointF(150, 100));
                g.DrawLine(MyDrawingPen, new PointF(150, 100), new PointF(200, 50));




            }
            else if (score > 2)
            {
                Graphics g = e.Graphics;
                Brush myDrawingBrush = new SolidBrush(Color.Cyan);
                Pen MyDrawingPen = new Pen(Color.Cyan);
                g.FillEllipse(myDrawingBrush, 60, 0, 20, 20);
                g.FillEllipse(myDrawingBrush, 115, 0, 20, 20);
                g.DrawLine(MyDrawingPen, new PointF(0, 60), new PointF(50, 30));
                g.DrawLine(MyDrawingPen, new PointF(50, 30), new PointF(150, 30));
                g.DrawLine(MyDrawingPen, new PointF(150, 30), new PointF(200, 60));

            }

        }

        private void checkSumEvent(object sender, EventArgs e)
        {

            if(score <= 2)
            {
                MessageBox.Show(
                "Survey Ended!" + Environment.NewLine +
                "Tw�j mood to: niescz�liwy!" + Environment.NewLine +
                "Zaraz pojawi si� ikonka twoich emocji:" + score
            );
            }
            else
            {
                MessageBox.Show(
                "Survey Ended!" + Environment.NewLine +
                "Tw�j mood to: szcz�sliwy!" + Environment.NewLine +
                "Zaraz pojawi si� ikonka twoich emocji:" + score
            );
            }




            panel1.Visible = true;
        }
    }
}