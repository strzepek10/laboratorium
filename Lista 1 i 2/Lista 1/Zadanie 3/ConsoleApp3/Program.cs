﻿using System;


namespace ConsoleApp3
{

    class Program
    {

        static void Main(string[] args)
        {
            
            Fridge fridge = new Fridge();
            Fridge fridge1 = new Fridge();

            Console.WriteLine($"Czy lodowka jest wlaczona? {fridge.Wlaczona}");
            Console.WriteLine($"Jakiego koloru jest lodówka? {fridge.Kolor}");
            Console.WriteLine();

            fridge.Wlacz();
            fridge.Kolor = "niebieski";

            Console.WriteLine($"Czy lodowka jest wlaczona? {fridge.Wlaczona}");
            Console.WriteLine($"Jakiego koloru jest lodówka? {fridge.Kolor}");
            Console.WriteLine();

            fridge.Wylacz();
            fridge.Kolor = "biały";

            Console.WriteLine($"Czy lodowka jest wlaczona? {fridge.Wlaczona}");
            Console.WriteLine($"Jakiego koloru jest lodówka? {fridge.Kolor}");
            Console.WriteLine();

            Microwave microwave = new Microwave();
            Microwave microwave1 = new Microwave();

            Console.WriteLine($"Czy mikrofalowka jest wlaczona? {microwave.Wlaczona}");
            Console.WriteLine($"Jakiego koloru jest mikrofalowka? {microwave.Kolor}");
            Console.WriteLine();

            microwave.Wlacz();
            microwave.Kolor = "niebieski";

            Console.WriteLine($"Czy mikrofalowka jest wlaczona? {microwave.Wlaczona}");
            Console.WriteLine($"Jakiego koloru jest mikrofalowka? {microwave.Kolor}");
            Console.WriteLine();

            microwave.Wylacz();
            microwave.Kolor = "biały";

            Console.WriteLine($"Czy mikrofalowka jest wlaczona? {microwave.Wlaczona}");
            Console.WriteLine($"Jakiego koloru jest mikrofalowka? {microwave.Kolor}");
            Console.WriteLine();

            Vacuum vacuum = new Vacuum();
            Vacuum vacuum1 = new Vacuum();

            Console.WriteLine($"Czy odkurzacz jest wlaczony? {vacuum.Wlaczona}");
            Console.WriteLine($"Jakiego koloru jest odkurzacz? {vacuum.Kolor}");
            Console.WriteLine();

            vacuum.Wlacz();
            vacuum.Kolor = "niebieski";

            Console.WriteLine($"Czy odkurzacz jest wlaczony? {vacuum.Wlaczona}");
            Console.WriteLine($"Jakiego koloru jest odkurzacz? {vacuum.Kolor}");
            Console.WriteLine();

            vacuum.Wylacz();
            vacuum.Kolor = "biały";

            Console.WriteLine($"Czy odkurzacz jest wlaczony? {vacuum.Wlaczona}");
            Console.WriteLine($"Jakiego koloru jest odkurzacz? {vacuum.Kolor}");
            Console.WriteLine();
        }
    }
}