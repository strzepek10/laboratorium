﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    
    class Fridge
    {
        public string rozmiar;
        public string glosnosc;
        public bool Wlaczona { get; set; } = false;
        public string Kolor { get; set; } = "czarna";
        public void Wlacz()
        {
            Wlaczona = true;
        }
        
        public void Wylacz()
        {
            Wlaczona = false;
        }
    }

    class Microwave
    {
        public string rozmiar;
        public string glosnosc;
        public bool Wlaczona { get; set; } = false;
        public string Kolor { get; set; } = "czarna";
        public void Wlacz()
        {
            Wlaczona = true;
        }

        public void Wylacz()
        {
            Wlaczona = false;
        }
    }

    class Vacuum
    {
        public string rozmiar;
        public string glosnosc;
        public bool Wlaczona { get; set; } = false;
        public string Kolor { get; set; } = "czarna";
        public void Wlacz()
        {
            Wlaczona = true;
        }

        public void Wylacz()
        {
            Wlaczona = false;
        }
    }



}
