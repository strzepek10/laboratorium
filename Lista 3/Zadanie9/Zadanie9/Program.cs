﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Zad9
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable mTable = new Hashtable();
            mTable.Add(100, "Paweł");
            mTable.Add(101, "Marcin");
            mTable.Add(102, "Dawid");


            foreach (int ID in mTable.Keys)
            {
                System.Console.WriteLine(ID);
            }

            System.Console.WriteLine(mTable[100]);
            System.Console.WriteLine(mTable[101]);
            System.Console.WriteLine(mTable[102]);

            Write();
            Read();


            SaveCurrentTime();
            ShowPreviousTime();
            Console.ReadKey();
        }
        const string filename = "przyklad.txt";
        static void Write()
        {
            StreamWriter sw = new StreamWriter(filename);
            string linia = "Witam serdecznie"; 
            sw.WriteLine(linia);
            sw.Close();
        }

        static void Read()
        {
            StreamReader sr = new StreamReader(filename);
            string linia = sr.ReadLine();
            Console.WriteLine(linia);
            sr.Close();
        }


        static void SaveCurrentTime()
        {
            FileStream fs = new FileStream("time.bin", FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs);

            bw.Write(DateTime.Now.Hour);
            bw.Write(DateTime.Now.Minute);

            bw.Close();
            fs.Close();
        }

        static void ShowPreviousTime()
        {
            FileStream fs = new FileStream("time.bin", FileMode.Open);
            BinaryReader br = new BinaryReader(fs);

            string previousTime = br.ReadInt32() + ":" + br.ReadInt32();
            Console.WriteLine(previousTime);
        }


    }
}