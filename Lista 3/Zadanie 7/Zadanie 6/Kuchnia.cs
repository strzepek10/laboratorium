﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie_6
{
    internal class Samochod
    {
        public string nazwa;
        public int zuzyciePaliwa;
        public int ileSekDosetki;
        public string rodzajNapedu;

        public Samochod(string nazwa1, int zuzyciePaliwa1, string rodzajNapedu1, int ileSekDosetki1)
        {
            zuzyciePaliwa = zuzyciePaliwa1;
            rodzajNapedu = rodzajNapedu1;
            ileSekDosetki = ileSekDosetki1;
            nazwa = nazwa1;
        }

        public bool ZuzywaniePaliwa()
        {
            if(zuzyciePaliwa <= 50)
            {
                return true;
            }
            return false;
        }

        public string JakieJestJejUzycie()
        {

            if (rodzajNapedu == "schładza")
            {
                Console.WriteLine("to fajnie");
                return "";
            }
            else if (rodzajNapedu == "podgrzewa")
            {
                Console.WriteLine("to dobrze");
                return "";
            }
            else if (rodzajNapedu == "czysci")
            {
                Console.WriteLine("to niedobrze");
                return "";
            }
            return "ok";
        }


        public static void Referencja(ref int value)
        {
            value++;
        }

        public static void Wyjscia(out int value)
        {
            value = 0;
            value++;
        }

        public static void Wartosc(int x)
        {
            x *= x;
            Console.WriteLine("Wartość w środku metody: {0}", x);
        }






    }
}
