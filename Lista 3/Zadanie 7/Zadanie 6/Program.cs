﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Zadanie_6;
using Zadanie6;

namespace Zadanie6
{
    class Program
    {
        static void Main(string[] args)
        {

            Samochod samochod1 = new Samochod("BMW", 10, "Benzyna", 10);
            Samochod samochod2 = new Samochod("Mercedes", 7, "Diesel", 13);
            Samochod samochod3 = new Samochod("Volvo", 12, "Gaz", 16);


            Console.WriteLine("{0} {1} {2}" , samochod1.ZuzywaniePaliwa(), samochod2.ZuzywaniePaliwa(), samochod3.ZuzywaniePaliwa());
            Console.WriteLine("{0} {1} {2}", samochod1.JakieJestJejUzycie(), samochod2.JakieJestJejUzycie(), samochod3.JakieJestJejUzycie());



            Console.WriteLine("Referencja");
            var myNumber = 0;
            Samochod.Referencja(ref myNumber);
            Samochod.Referencja(ref myNumber);
            Samochod.Referencja(ref myNumber);
            Console.WriteLine(myNumber);



            Console.WriteLine("Wyjscia");
            var myNumber2 = 0;
            Samochod.Wyjscia(out myNumber2);
            Samochod.Wyjscia(out myNumber2);
            Samochod.Wyjscia(out myNumber2);
            Console.WriteLine(myNumber2);



            Console.WriteLine("Wartosc");
            int x = 11;
            Samochod.Wartosc(x);





        }

    }
}