﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace CSharp
{
    public class Program
    {
        public delegate void MetodaDelegat();
        static void Main(string[] args)
        {
            MetodaDelegat jakasMetoda = MetodaDel;
            jakasMetoda.Invoke();

        }

        static void MetodaDel()
        {
            Console.WriteLine("UWAGA - Dziś w nocy mocne opady deszczu! Czy chcesz zapisac numer? (tak, nie)");
            string userInput1 = Console.ReadLine();

            if (userInput1 == "tak")
            {
                Console.WriteLine("Podaj Numer:");
                string userInput2 = Console.ReadLine();
                Console.WriteLine("{0} {1} {2}", "Numer", userInput2, "został dopisany do bazy danych");
                Console.ReadKey();
            }
            else if (userInput1 == "nie")
            {
                Console.WriteLine("Brak zapisu");
            }


        }

    }

}